Source: senlin-dashboard
Section: python
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
 Michal Arbet <michal.arbet@ultimum.io>,
Build-Depends:
 debhelper-compat (= 11),
 dh-python,
 openstack-pkg-tools,
 python3-all,
 python3-pbr,
 python3-setuptools,
 python3-sphinx,
Build-Depends-Indep:
 openstack-dashboard,
 python3-babel,
 python3-coverage,
 python3-django,
 python3-django-compressor,
 python3-iso8601,
 python3-openstackdocstheme,
 python3-senlinclient,
 python3-sphinxcontrib.apidoc,
 python3-yaml,
 testrepository,
Standards-Version: 4.6.1
Homepage: https://github.com/stackforge/senlin-dashboard
Vcs-Git: https://salsa.debian.org/openstack-team/horizon-plugins/senlin-dashboard.git
Vcs-Browser: https://salsa.debian.org/openstack-team/horizon-plugins/senlin-dashboard

Package: python3-senlin-dashboard
Architecture: all
Depends:
 openstack-dashboard,
 python3-babel,
 python3-django,
 python3-django-compressor,
 python3-iso8601,
 python3-pbr,
 python3-senlinclient,
 python3-yaml,
 ${misc:Depends},
 ${python3:Depends},
Description: clustering service for OpenStack clouds - dashboard plugin
 Senlin is a clustering service for OpenStack clouds. It creates and operates
 clusters of homogenous objects exposed by other OpenStack services. The goal
 is to make the orchestration of collections of similar objects easier.
 .
 Senlin provides RESTful APIs to users so that they can associate various
 policies to a cluster. Sample policies include placement policy, load
 balancing policy, health policy, scaling policy, update policy and so on.
 .
 Senlin is designed to be capable of managing different types of objects. An
 object's lifecycle is managed using profile type implementations, which are
 themselves plugins.
 .
 This package contains the OpenStack dashboard (aka Horizon) plugin.
